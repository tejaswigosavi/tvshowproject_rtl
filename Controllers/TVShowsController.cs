﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication1.Data;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TVShowsController : ControllerBase
    {

        private IShowRepository _showRepository;
        private readonly ILogger<TVShowsController> _logger;

        public TVShowsController(ILogger<TVShowsController> logger, IShowRepository showRepository)
        {
            _logger = logger;
            _showRepository = showRepository;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var result = await _showRepository.GetTVShows();
            if(result==null)
            return Ok(result);
        }
    }
}
