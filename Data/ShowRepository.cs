﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApplication1.Data
{
    public class ShowRepository: IShowRepository
    {
        public async Task<List<TVShowsModel>> GetTVShows()
        {
            try
            {
                HttpClient http = new HttpClient();
                List<TVShowsModel> TvShowsList = new List<TVShowsModel>();
                //for calling get

                var data2 = http.GetAsync(UrlConstant.TvShowUrl).Result.Content.ReadAsStringAsync().Result;
                Newtonsoft.Json.Linq.JArray jsonVal = Newtonsoft.Json.Linq.JValue.Parse(data2) as Newtonsoft.Json.Linq.JArray;
                dynamic albums = jsonVal;
                foreach (dynamic album in albums)
                {

                    TVShowsModel tvshow = new TVShowsModel();
                    tvshow.id = album.show.id;
                    tvshow.name = album.show.name;
                    tvshow.CastsList = new List<CastModel>();
                    string url = UrlConstant.CastUrl + tvshow.id + "/cast";
                    var castJson = http.GetAsync(url).Result.Content.ReadAsStringAsync().Result;
                    Newtonsoft.Json.Linq.JToken jsonCastVal = Newtonsoft.Json.Linq.JToken.Parse(castJson) as Newtonsoft.Json.Linq.JToken;

                    if (jsonCastVal.HasValues)
                    {
                        dynamic Casts = jsonCastVal;
                        foreach (dynamic cast in Casts)
                        {
                            CastModel castModel = new CastModel
                            {
                                id = cast.person.id,
                                name = cast.person.name,
                                Birthdate = cast.person.birthday
                            };
                            tvshow.CastsList.Add(castModel);
                        }
                    }
                    tvshow.CastsList.Sort((x, y) => Convert.ToDateTime(y.Birthdate).CompareTo(Convert.ToDateTime(x.Birthdate)));
                    TvShowsList.Add(tvshow);

                }

                //httpResponse.Content = TvShowsList;
                return TvShowsList;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
