﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class TVShowsModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<CastModel> CastsList { get; set; }
    }
}
