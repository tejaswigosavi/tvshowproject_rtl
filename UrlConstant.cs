﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class UrlConstant
    {
        public const string TvShowUrl = "http://api.tvmaze.com/search/shows?q=scrapes";
        public const string CastUrl = "http://api.tvmaze.com/shows/";
    }
}
